FROM microsoft/dotnet:latest
MAINTAINER YQme (chris.fernandez@yqme.com.au)
# Install required packages

ENV NPM_CONFIG_LOGLEVEL error
ENV NODE_VERSION 8.12.0

RUN curl -SLO "https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-linux-x64.tar.gz" \
  && tar -xzf "node-v$NODE_VERSION-linux-x64.tar.gz" -C /usr/local --strip-components=1 \
  && ln -s /usr/local/bin/node /usr/local/bin/nodejs  \
  && rm "node-v$NODE_VERSION-linux-x64.tar.gz"

RUN dotnet --info
RUN node -v